FROM openjdk:11-jdk

ARG VCS_REF
ARG VERSION
ARG BUILD_DATE

LABEL \
    org.label-schema.schema-version="1.0" \
    org.label-schema.name="nx-sonarscanner-base" \
    org.label-schema.description="Image can be used in a buildpipeline to analyse an nx project with 'sonar-scanner'." \
    org.label-schema.vcs-url="https://gitlab.com/devgem/docker-nx-sonarscanner-base" \
    org.label-schema.vendor="DevGem" \
    org.label-schema.vcs-ref=$VCS_REF \
    version=$VERSION \
    build-date=$BUILD_DATE

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Install NodeJS
RUN apt-get update && \
    apt-get -y install --no-install-recommends curl gnupg && \
    curl -sL https://deb.nodesource.com/setup_14.x  | bash - && \
    apt-get -y install --no-install-recommends nodejs && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install sonarscanner
RUN npm install -g sonarqube-scanner

# Install nx
RUN npm install -g nx

RUN \
    echo '' && \
    echo '********' && \
    echo 'VERSIONS' && \
    echo '********' && \
    echo '' && \
    echo 'java:' && \
    java -version && \
    echo '' && \
    echo 'nodejs:' && \
    node -v && \
    echo '' && \
    echo '********' && \
    echo ''
