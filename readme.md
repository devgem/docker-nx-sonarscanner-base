# Nx Sonarscanner

[![nx-sonarscanner](https://img.shields.io/badge/nx--sonarscanner-0.6-brightgreen.svg)](https://hub.docker.com/r/devgem/nx-sonarscanner-base/tags/)
 [![sonarscanner](https://img.shields.io/badge/sonarscanner-2.8.0-brightgreen.svg)](https://www.nuget.org/packages/nx-sonarscanner)
 [![java](https://img.shields.io/badge/java-11.0.10-brightgreen.svg)](https://www.java.com/)
 [![nx](https://img.shields.io/badge/nx-11.2.10-brightgreen.svg)](https://nx.dev/)
 [![nodejs](https://img.shields.io/badge/nodejs-14.15.4-brightgreen.svg)](https://nodejs.org/en/)

[![Docker Stars](https://img.shields.io/docker/stars/devgem/nx-sonarscanner-base.svg)](https://hub.docker.com/r/devgem/nx-sonarscanner-base 'DockerHub')
 [![Docker Pulls](https://img.shields.io/docker/pulls/devgem/nx-sonarscanner-base.svg)](https://hub.docker.com/r/devgem/nx-sonarscanner-base 'DockerHub')
 [![Docker Layers](https://images.microbadger.com/badges/image/devgem/nx-sonarscanner-base.svg)](https://microbadger.com/images/devgem/nx-sonarscanner-base)
 [![Docker Version](https://images.microbadger.com/badges/version/devgem/nx-sonarscanner-base.svg)](https://hub.docker.com/r/devgem/nx-sonarscanner-base)

This docker image can be used as a base image for analyzing nx and node projects in a CI pipeline.

To build the image:

```shell
export NX_SONARSCANNER_VERSION="0.6"
docker build -t devgem/nx-sonarscanner-base:$NX_SONARSCANNER_VERSION --build-arg VERSION="$NX_SONARSCANNER_VERSION" --build-arg VCS_REF=`git rev-parse --short HEAD` --build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` .
```

## Usage

```Dockerfile
# use the image as a base image to analyze your projects
FROM devgem/nx-sonarscanner-base:0.6

ARG PIPELINEID
ARG SONARQUBE_TOKEN

WORKDIR /src
COPY . .

RUN sonarscanner-scanner -Dsonar.host.url=https://your-sonarserver.com -Dsonar.login=${SONARQUBE_TOKEN} -Dsonar.projectKey="your-projectkey" -Dsonar.projectVersion="${PIPELINEID}"
```

Replace the whole `your-*` by your stuff. Replace the value of `sonar.host.url` with the url of your SonarQube instance. Add your analysis arguments with `-Dpropertyname=propertyvalue`.

## Versions

Pre-built images are available on Docker Hub:

| image | date | sonarscanner | openjdk | nx | nodejs | remarks |
|-----|-----|-----|-----|-----|-----|-----|
| [`devgem/nx-sonarscanner-base:0.6`](https://hub.docker.com/r/devgem/nx-sonarscanner-base/tags/) | 20210205 | 2.8.0 | 11.0.10 | 11.2.10 | 14.15.4 | |
| [`devgem/nx-sonarscanner-base:0.5`](https://hub.docker.com/r/devgem/nx-sonarscanner-base/tags/) | 20210104 | 2.8.0 | 11.0.9.1 | 11.0.20 | 14.15.3 | |
| [`devgem/nx-sonarscanner-base:0.4`](https://hub.docker.com/r/devgem/nx-sonarscanner-base/tags/) | 20201217 | 2.8.0 | 11.0.9.1 | 11.0.10 | 14.15.2 | |
| [`devgem/nx-sonarscanner-base:0.3`](https://hub.docker.com/r/devgem/nx-sonarscanner-base/tags/) | 20201124 | 2.8.0 | 11.0.9.1 | 10.4.4 | 14.15.1 | |
| [`devgem/nx-sonarscanner-base:0.2`](https://hub.docker.com/r/devgem/nx-sonarscanner-base/tags/) | 20201117 | 2.8.0 | 11.0.9.1 | 10.4.2 | 14.15.1 | |
| [`devgem/nx-sonarscanner-base:0.1`](https://hub.docker.com/r/devgem/nx-sonarscanner-base/tags/) | 20201117 | 2.8.0 | 11.0.9.1 | 10.4.1 | 14.15.1 | |

## Updating on MicroBadger

```shell
curl -X POST https://hooks.microbadger.com/images/devgem/nx-sonarscanner-base/yIfjwvvjU7UBs1pURaIxVREHpOU=
```
